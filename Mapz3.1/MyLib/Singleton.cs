﻿using System;

namespace Singleton {
    public class Database {
        private static Database IsCreated = null;
        private static int id;
        private Database() {
            Random rnd = new Random();
            id = rnd.Next();
            
        }
        public static Database Connect() {
            if (IsCreated == null) {
                IsCreated = new Database();
            }
            Console.WriteLine($"Database with id {id} connected secsessfuly!");
            return IsCreated;
        }
    }

}
