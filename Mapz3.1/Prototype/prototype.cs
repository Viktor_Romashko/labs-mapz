﻿using System;

namespace Prototype {
    public class Form {
        public int Age;
        public string Name;
        public IdInfo IdInfo;

        public Form Clone() {
            Form clone = (Form)this.MemberwiseClone();
            clone.IdInfo = new IdInfo(IdInfo.IdNumber);
            clone.Name = String.Copy(Name);
            return clone;
        }
    }
    public class IdInfo {
        public int IdNumber;
        public IdInfo(int idNumber) {
            this.IdNumber = idNumber;
        }
    }
}
