﻿using System;

namespace AbstractFactory {
    public interface FileOpener {
        void Create();
    }
    public class Word : FileOpener {
        public Word() { }
        public void Create() {
            Console.WriteLine("File is opened with .docx");
        }
    }
    public class Pdf : FileOpener {
        public Pdf() { }
        public void Create() {
            Console.WriteLine("File is opened with .pdf");
        }
    }
    public class Pttx : FileOpener {
        public Pttx() { }
        public void Create() {
            Console.WriteLine("File is opened with .pttx");
        }
    }
    public class Paragraf {
        public string format;
        public string name;
        public int size;

        public Paragraf(string n, string n2, int s) {
            format = n;
            name = n2;
            size = s;
        }
    }
}
