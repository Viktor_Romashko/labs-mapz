﻿using System;
using Singleton;
using AbstractFactory;
using Prototype;


namespace Mapz3._1 {
    class Program {
        
        static void Open(FileOpener flop) {
            flop.Create();
        }
        static void Main(string[] args) {
            Database MyDb1 = Database.Connect();
            Database MyDb2 = Database.Connect();
            Database MyDb3 = Database.Connect();

            Paragraf file1 = new Paragraf("pdf","C",5);
            Paragraf file2 = new Paragraf("docx","C++",9);
            Paragraf file3 = new Paragraf("pttx","C#",8);

            Pttx presentation = new Pttx();
            Pdf article = new Pdf();
            Word document = new Word();

        
            Console.WriteLine("Choose paragraph:\n");
            Console.WriteLine("1.C\n");
            Console.WriteLine("2.C++\n");
            Console.WriteLine("3.C#\n");
            int choose = Convert.ToInt32(Console.ReadLine());

            switch (choose) {
                case 1:
                    Open(presentation);
                    break;
                case 2:
                    Open(article);
                    break;
                case 3:
                    Open(document);
                    break;
            }

            Form Viktor = new Form();
            Form Pavlo = new Form();
            Viktor.Age = 19;
            Viktor.Name = "Viktor";
            Viktor.IdInfo = new IdInfo(666);

            Pavlo = Viktor.Clone();
            Console.WriteLine("First login");
            Console.WriteLine($"Viktor info:{Viktor.Age} years,{Viktor.Name}-name,{Viktor.IdInfo}-id\n");
            Console.WriteLine("Cloning Viktor to Pavlo\n");
            Console.WriteLine($"Pavlo info:{Pavlo.Age} years,{Pavlo.Name}-name,{Pavlo.IdInfo}-id\n");
            Console.WriteLine("\nChanging Viktor\n");
          
            Viktor.Age = 26;
            Viktor.Name = "Viktor 2";
            Viktor.IdInfo = new IdInfo(6);
            Console.WriteLine("Changed first login\n");
            Console.WriteLine("New Viktor\n");
            Console.WriteLine($"Viktor info:{Viktor.Age} years,{Viktor.Name}-name,{Viktor.IdInfo}-id\n");
            Console.WriteLine("Pavlo was copied later and didn`t change\n");
            Console.WriteLine($"Pavlo info:{Pavlo.Age} years,{Pavlo.Name}-name,{Pavlo.IdInfo}-id\n");


        }
    }
}
