﻿using System;
using Strategy;
using Mediator;
using Observer;

namespace ConsoleApp2 {
    class Program {
        static void Main(string[] args) {
            LogIn logIn = new LogIn();
            LogOut logOut = new LogOut();
            new MediatorIn(logIn, logOut);

            Console.WriteLine("Student starting log in operation");
            logIn.DoA();

            Console.WriteLine();

            Console.WriteLine("Student started log out operation");
            logOut.DoD();


            Manager man = new Manager();
            Idownload id = new DownPDF();
            man.SetDownType(id);
            man.ExecDown();
            id = new DownTXT();
            man.SetDownType(id);
            man.ExecDown();
            id = new DownDOCX();
            man.SetDownType(id);
            man.ExecDown();
            
            var subject = new News();
            var observerA = new StudentA();
            subject.Attach(observerA);

            var observerB = new StudentB();
            subject.Attach(observerB);

            subject.Logic();
            subject.Logic();

            subject.Detach(observerB);

            subject.Logic();
        }
    }
}