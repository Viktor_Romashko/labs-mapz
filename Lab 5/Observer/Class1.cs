﻿using System;
using System.Collections.Generic;

namespace Observer {
    public interface IObserver {
        void Update(INews news);
    }
    public interface INews {
        void Attach(IObserver observer);
        void Detach(IObserver observer);
        void Notify();
    }
    public class News : INews {
        public int State { get; set; } = -0;
        private List<IObserver> _observers = new List<IObserver>();
        public void Attach(IObserver observer) {
            Console.WriteLine("News: Attached a student.");
            this._observers.Add(observer);
        }

        public void Detach(IObserver observer) {
            this._observers.Remove(observer);
            Console.WriteLine("News: Detached a student.");
        }
        public void Notify() {
            Console.WriteLine("News: Notifying students");
            foreach (var observer in _observers) {
                observer.Update(this);
            }
        }
        public void Logic() {
            Console.WriteLine("\nNews: Sending something important.");
            this.State = new Random().Next(0, 10);

            Console.WriteLine("News: My state has just changed to: " + this.State);
            this.Notify();
        }
    }
    public class StudentA : IObserver {
        public void Update(INews news) {            
            if ((news as News).State < 3) {
                Console.WriteLine("Student A: Reacted");
            }
        }
    }
    public class StudentB : IObserver {
        public void Update(INews news) {
            if ((news as News).State == 0 || (news as News).State >= 2) {
                Console.WriteLine("StudentB: Reacted");
            }
        }
    }
    
}