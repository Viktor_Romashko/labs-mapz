﻿using System;

namespace Mediator {
     public interface IMediator {
        void Notify(object sender, string ev);
    }
     public class MediatorIn : IMediator {
        private LogIn _logIn;
        private LogOut _logOut;
        public MediatorIn(LogIn logIn, LogOut logOut) {
            this._logIn = logIn;
            this._logIn.SetMediator(this);
            this._logOut = logOut;
            this._logOut.SetMediator(this);
        } 

        public void Notify(object sender, string ev) {
            if (ev == "A") {
                Console.WriteLine("Mediator reacts on A and triggers Loging out:");
                this._logOut.DoC();
            }
            if (ev == "D") {
                Console.WriteLine("Mediator reacts on D :");
                this._logIn.DoB();
                this._logOut.DoC();
            }
        }
    }
     public class BaseDoing {
        protected IMediator _mediator;

        public BaseDoing(IMediator mediator = null) {
            this._mediator = mediator;
        }
        public void SetMediator(IMediator mediator) {
            this._mediator = mediator;
        }
    }
     public class LogIn : BaseDoing {
        public void DoA() {
            Console.WriteLine("Loging in");
            this._mediator.Notify(this, "A");
        }
        public void DoB() {
            Console.WriteLine("Person is autentificated");
            this._mediator.Notify(this, "B");
        }
    }

    public class LogOut : BaseDoing {
        public void DoC() {
            Console.WriteLine("Loging out");
            this._mediator.Notify(this, "C");
        }
        public void DoD() {
            Console.WriteLine("Person is loged out");
            this._mediator.Notify(this, "D");
        }
    }
}