﻿using System;

namespace Strategy {
    public interface Idownload {
        void download();
    }

    public class DownPDF : Idownload {
       public void download() {
            Console.WriteLine("Downloaded in PDF-format\n");
        }
    }
    public class DownDOCX : Idownload {
        public void download() {
            Console.WriteLine("Downloaded in DOCX-format\n");
        }
    }
    public class DownTXT : Idownload {
        public void download() {
            Console.WriteLine("Downloaded in TXT-format\n");
        }
    }

    public class Manager {
        private Idownload _download;

        public void SetDownType(Idownload download) {
            _download = download;
        }

        public void ExecDown() {
            _download.download();
        }
    }
}