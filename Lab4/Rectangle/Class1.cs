﻿using System;
using System.Data;
using System.Security.Cryptography;

namespace lib
{
    public class FasadeOfForm {
        private User cur_user;
        private Database cur_database;
        private Server cur_server;
        private Form cur_form;

        public FasadeOfForm(User u,Database d,Server s,Form f) {
            cur_user = u;
            cur_database = d;
            cur_server = s;
            cur_form = f;
        }

        public void EnterTheForm(string name,int id) {
            cur_user.IsAtnt(name, id);
            cur_database.Connect(cur_database.Table);
            cur_server.LoadForm(cur_form.Name);
            cur_form.EnterForm();
        }

    }

    public class User {
        protected int id;
        protected string name;
        
        public User(string name,int id) {
            this.name = name;
            this.id = id;
        }

        public bool IsAtnt(string name, int id) {
            if (this.name == name && this.id == id) return true;
            else return false;
        }
    }

    public class Database {
        private string name;
        private string table;

        public string Table {
            get => table;
            set => table = value;
        }
        
        public Database(string name,string table) {
            this.name = name;
            this.table = table;
        }

        public void Connect(string table) {
            Console.WriteLine($"Connected to {table}");
        }
    }

    public class Server {
        public void LoadForm(string name) {
            Console.WriteLine($"Form {name} has been loaded!");
        }
    }

    public class Form {
        private string name;

        public string Name {
            get => name;
            set => name = value;
        }
        public Form(string name) {
            this.name = name;
        }

        public void EnterForm() {
            Console.WriteLine("Form was entered");
        }
    }

    public class Student {
        public FasadeOfForm fasade;

        public Student(FasadeOfForm a) {
            fasade = a;
        }
        
        public void EnterForm() {
            fasade.EnterTheForm("Viktor",1);
        }
    }
}