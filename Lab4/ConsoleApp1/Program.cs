﻿using System;
using lib;
using Deco;
using Proxy;



namespace ConsoleApp1 {
    class Program {
        static void Main(string[] args) {
            User cur_user = new User("Viktor",1);
            Database cur_database = new Database("db","form");
            Server cur_server =new Server();
            Form cur_form = new Form("University");

            FasadeOfForm Fasade =new FasadeOfForm(cur_user, cur_database, cur_server, cur_form);

            Student student = new Student(Fasade);
            student.EnterForm();
            
            Console.WriteLine("\n");
            Notification ntf = new UserNotify();
            
            ntf.notify();
            Console.WriteLine("\n");
            ntf = new TelegramNotifier(ntf);
            ntf.notify();
            Console.WriteLine("\n");
            ntf = new EmailNotifier(ntf);
            ntf.notify();
            Console.WriteLine("\n");
            ntf = new VNSNotifier(ntf);
            ntf.notify();
            Console.WriteLine("\n");
            ntf = new UserNotify();
            ntf = new TelegramNotifier(ntf);
            ntf = new VNSNotifier(ntf);
            ntf.notify();
            Console.WriteLine("\n");
            
            Console.WriteLine("\n");
            StudentUser studentUser = new StudentUser();

            Console.WriteLine("Client: Executing the client code with a real subject:");
            RealIdata realSubject = new RealIdata();
            studentUser.ClientRquesting(realSubject);

            Console.WriteLine();

            Console.WriteLine("Client: Executing the same client code with a proxy:");
            Proxy.Proxy proxy = new Proxy.Proxy(realSubject);
            studentUser.ClientRquesting(proxy);



        }
    }
}