﻿using System;

namespace Deco {
    public interface Notification {
        public void notify();
    }

    public class UserNotify : Notification {
        public void notify() {
            Console.WriteLine("User will be notified\n");
        }
    }

    public abstract class NotifierDeco : Notification {
        protected Notification ntfct;

        public NotifierDeco(Notification notifier) {
            this.ntfct = notifier;
        }

        public virtual void notify() {
            ntfct.notify();
        }
    }

    public class TelegramNotifier : NotifierDeco {
        public TelegramNotifier(Notification notifier) : base(notifier) {
        }

        public override void notify() {
            base.notify();
            Console.WriteLine("Telegram message was sent to you.\n");
        }

    }
    public class VNSNotifier : NotifierDeco {
        public VNSNotifier(Notification notifier) : base(notifier) {
        }

        public override void notify() {
            base.notify();
            Console.WriteLine("VNS notification was sent to you.\n");
        }

    }
    public class EmailNotifier : NotifierDeco {
        public EmailNotifier(Notification notifier) : base(notifier) {
        }
        public override void notify() {
            base.notify();
            Console.WriteLine("An e-mail was sent to you.\n");
        }

    }
}