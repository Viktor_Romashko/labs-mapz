﻿using System;

namespace Proxy {
    public interface Idata {
        void Request();
    }
    public class RealIdata : Idata {
        public void Request() {
            Console.WriteLine("RealDatbase: Handling Request.");
        }
    }
    public class Proxy : Idata {
        private RealIdata realIdata;
        
        public Proxy(RealIdata realIdata) {
            this.realIdata = realIdata;
        }
        
        public void Request() {
            if (this.CheckAccess()) {
                this.realIdata.Request();
                this.LogAccess();
            }
        }
        public bool CheckAccess() {
            Console.WriteLine("Proxy(Not real Databse): Checking access prior to firing a real request.");
            return true;
        }
        
        public void LogAccess() {
            Console.WriteLine("Proxy: Logging the time of request.");
        }
    }
    public class StudentUser {
        public void ClientRquesting(Idata idata) {
            idata.Request();
        }
    }
}